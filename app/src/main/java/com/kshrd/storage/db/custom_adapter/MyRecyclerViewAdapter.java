package com.kshrd.storage.db.custom_adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kshrd.storage.R;
import com.kshrd.storage.db.entity.Person;
import com.kshrd.storage.db.myclick.MyClickItems;

import java.util.List;


public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder> {

    private List<Person> personList;
    Context context;
    MyClickItems myClickItems;

    public MyRecyclerViewAdapter(List<Person> personList) {
        this.personList = personList;
        this.context = context;
        Log.e("ooooo", this.personList.size() + "");
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_cardview,
                parent, false);

        return new MyViewHolder(view);
    }

    public void setListener(MyClickItems myClickItem){
        this.myClickItems = myClickItem;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Person person = personList.get(position);

        Log.e("ooooo", person.toString());

        holder.tvID.setText(person.getId() + "");
        holder.tvName.setText(person.getName());
        holder.tvGender.setText(person.getGender());

    }

    @Override
    public int getItemCount() {
        return personList.size();
    }

    public void removePerson(int pos){
        this.personList.remove(pos);
        notifyItemRemoved(pos);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvID;
        TextView tvName;
        TextView tvGender;
        ImageButton ibDelete;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvID = (TextView) itemView.findViewById(R.id.tvId);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvGender = (TextView) itemView.findViewById(R.id.tvGender);
            ibDelete = (ImageButton) itemView.findViewById(R.id.ibDelete);

            ibDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            myClickItems.onDeleteItem(getAdapterPosition());
        }
    }

}
