package com.kshrd.storage.sharepreference;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kshrd.storage.R;

import java.lang.reflect.Type;
import java.util.List;

public class ShowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        TextView tvLabel = (TextView) findViewById(R.id.tvLabel);
        String str = "";

        SharedPreferences sharedPreferences = getSharedPreferences(
                Constant.USER_PREFERENCE, MODE_PRIVATE
        );

        int userId = sharedPreferences.getInt(Constant.USER_ID, 0);
        String userName = sharedPreferences.getString(Constant.USER_NAME, "N/A");
        String userFromJson = sharedPreferences.getString(Constant.USER, "N/A");
        String userListFromJson = sharedPreferences.getString(Constant.USER_LIST, "N/A");

        str += "ID -> " + userId + "\n Name -> " + userName + "\n";

        User user;
        if (!userFromJson.equals("N/A")){
            user = new Gson().fromJson(userFromJson, User.class);
            str += "UserObject -> " + user.getName() + "\n";
        }

        List<User> userList;
        if (!userListFromJson.equals("N/A")){
            Type type = new TypeToken<List<User>>(){}.getType();
            userList = new Gson().fromJson(userListFromJson, type);
            str += "Number of User -> " + userList.size();
        }

        tvLabel.setText(str);


    }
}
