package com.kshrd.storage.db;

/**
 * Created by pirang on 6/7/17.
 */

public class DbConstant {

    public static final String DB_NAME = "contact_db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_PERSON = "tbl_person";
    public static final String PERSON_ID = "person_id";
    public static final String PERSON_NAME = "person_name";
    public static final String PERSON_GENDER = "person_gender";

    public static final String CREATE_TABLE_PERSON = "CREATE TABLE " +
            TABLE_PERSON + "(" +
            PERSON_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            PERSON_NAME + " TEXT NOT NULL," +
            PERSON_GENDER + " TEXT)";

}
